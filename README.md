Account API service
===================

This should be implemented with an API and database.
Business Requirements:
* Should not allow more than one user with the same email address
* Zip Pay allows credit for up to $1000, so if monthly salary - monthly expenses is less than
$1000 we should not create an account for the user and return an error

API requirements
----------------
1. The following Endpoints are required:
    * Create user
        * Fields to be provided and persist
            * Name
            * Email Address (Must be unique)
            * Monthly Salary (Must be a positive number - for simplicity, pretend there is no such thing as income tax)
            * Monthly Expenses (Must be a positive number)
    
    * List users
    * Get user
    * Create an account
        * Given a user, create an account
        * Should take into account the business requirements above
        * Up to you to decide the appropriate fields to persist here
    * List Accounts
2. Should be robust and include relevant response codes for different scenarios
3. No need to implement any kind of auth for this test

Implementation
--------------
API is defined as a Protobuf files (see ./src/main/protobuf)
As a storage MongoDB is used with ReactiveMongo DB client. Server-side is based on gRPC interactions and generated 
from Protobuf service definitions

Starting
--------
To compile and start application go to root directory and


    % sbt package test
    
    [info] Run completed in 435 milliseconds.
    [info] Total number of tests run: 17
    [info] Suites: completed 2, aborted 0
    [info] Tests: succeeded 17, failed 0, canceled 0, ignored 0, pending 0
    [info] All tests passed.
    [success] Total time: 4 s, completed 2 Dec. 2019, 2:45:51 pm


To start application within dockerized infrastructure (you need to have docker and docker-compose installed on your machine)


    % sbt dockerComposeUp


In console output you will see the following table with all info to access your app



    The following endpoints are available for your local instance: 602007
    +---------+-----------------+------------------------------------------------------------------+--------------+----------------+--------------+---------+
    | Service | Host:Port       | Tag Version                                                      | Image Source | Container Port | Container Id | IsDebug |
    +=========+=================+==================================================================+==============+================+==============+=========+
    | mongo   | localhost:27017 | 1a9478d8188d6be31dd2e8de076d402edf20446e54933aad7ff49f5b457d486c | defined      | 27017          | 32de06d65a43 |         |
    | server  | localhost:32842 | 0.1                                                              | build        | 9080           | 6326d6d99e59 |         |
    | server  | localhost:32843 | 0.1                                                              | build        | 5005           | 6326d6d99e59 | DEBUG   |
    +---------+-----------------+------------------------------------------------------------------+--------------+----------------+--------------+---------+


Integration testing
-------------------
Integration tests is supposed to be run on top of doker infrastructure (you need to have docker and docker-compose installed on your machine) To do this just execute


    % sbt dockerComposeTest


