package ms.konovalov.zip

import io.grpc.{Status, StatusRuntimeException}
import org.scalatest.{Assertion, AsyncFreeSpec, Matchers, Succeeded}

import scala.concurrent.{ExecutionContext, Future}

class AccountServiceTest extends AsyncFreeSpec with Matchers {

  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  "create account" - {
    "should create an account for a given user" in {
      val userDao = new UserDaoFake
      implicit val userService: UserServiceImpl = new UserServiceImpl(userDao)
      implicit val accountService: AccountServiceImpl = new AccountServiceImpl(userDao, new AccountDaoFake)
      createUserAndAccount() map { result =>
        result.id.length should be > 0
      }
    }

    "should fail for user with existing account" in {
      val userDao = new UserDaoFake
      implicit val userService: UserServiceImpl = new UserServiceImpl(userDao)
      implicit val accountService: AccountServiceImpl = new AccountServiceImpl(userDao, new AccountDaoFake)
      recoverToExceptionIf[StatusRuntimeException] {
        createUserAndAccount() flatMap { account =>
          accountService.createAccount(CreateAccountRequest(account.id))
        }
      } map { exception => exception.getStatus.getCode shouldBe Status.ALREADY_EXISTS.getCode }
    }

    "should fail for user with not enough income" in {
      val userDao = new UserDaoFake
      implicit val userService: UserServiceImpl = new UserServiceImpl(userDao)
      implicit val accountService: AccountServiceImpl = new AccountServiceImpl(userDao, new AccountDaoFake)
      recoverToExceptionIf[StatusRuntimeException] {
        createUserAndAccount(income = 100, expenses = 100)
      } map { exception => exception.getStatus.getCode shouldBe Status.FAILED_PRECONDITION.getCode }
    }
  }

  "list accounts" - {
    "should be empty for empty data" in {
      val userDao = new UserDaoFake
      implicit val userService: UserServiceImpl = new UserServiceImpl(userDao)
      implicit val accountService: AccountServiceImpl = new AccountServiceImpl(userDao, new AccountDaoFake)
      accountService.listAccount(ListAccountRequest(pageSize = 5)) map { result =>
        all(
          result.accounts should be(empty),
          result.nextPageToken shouldBe ""
        )
      }
    }

    "should return all data if the page size is bigger" in {
      val userDao = new UserDaoFake
      implicit val userService: UserServiceImpl = new UserServiceImpl(userDao)
      implicit val accountService: AccountServiceImpl = new AccountServiceImpl(userDao, new AccountDaoFake)
      createUserAndAccount() flatMap { _ =>
        createUserAndAccount(name = "bill", email = "email2") flatMap { _ =>
          accountService.listAccount(ListAccountRequest(pageSize = 5)) map { result =>
            all(
              result.accounts should have size 2,
              result.nextPageToken shouldBe ""
            )
          }
        }
      }
    }

    "should return data for first page" in {
      val userDao = new UserDaoFake
      implicit val userService: UserServiceImpl = new UserServiceImpl(userDao)
      implicit val accountService: AccountServiceImpl = new AccountServiceImpl(userDao, new AccountDaoFake)
      createUserAndAccount() flatMap { _ =>
        createUserAndAccount(name = "bill", email = "email2") flatMap { _ =>
          createUserAndAccount(name = "cam", email = "email3") flatMap { _ =>
            createUserAndAccount(name = "dan", email = "email4") flatMap { _ =>
              accountService.listAccount(ListAccountRequest(pageSize = 2)) map { result =>
                result.accounts should have size 2
              }
            }
          }
        }
      }
    }

    "should return data for next page" in {
      val userDao = new UserDaoFake
      implicit val userService: UserServiceImpl = new UserServiceImpl(userDao)
      implicit val accountService: AccountServiceImpl = new AccountServiceImpl(userDao, new AccountDaoFake)
      createUserAndAccount() flatMap { _ =>
        createUserAndAccount(name = "bill", email = "email2") flatMap { _ =>
          createUserAndAccount(name = "cam", email = "email3") flatMap { _ =>
            createUserAndAccount(name = "dan", email = "email4") flatMap { _ =>
              createUserAndAccount(name = "ellie", email = "email5") flatMap { _ =>
                accountService.listAccount(ListAccountRequest(pageSize = 2)) flatMap { page1 =>
                  accountService.listAccount(ListAccountRequest(pageSize = 2, pageToken = page1.nextPageToken)) flatMap { page2 =>
                    accountService.listAccount(ListAccountRequest(pageSize = 2, pageToken = page2.nextPageToken)) map { page3 =>
                      all(
                        page1.accounts should have size 2,
                        page2.accounts should have size 2,
                        page3.accounts should have size 1,
                        page2.accounts should not contain page1.accounts(0),
                        page2.accounts should not contain page1.accounts(1),
                        page2.accounts should not contain page3.accounts(0),
                        page1.accounts should not contain page3.accounts(0),
                      )
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    "should fail on incorrect page token" in {
      val userService: UserServiceImpl = new UserServiceImpl(new UserDaoFake)
      userService.createUser(CreateUserRequest(name = "adam", email = "email")) flatMap { _ =>
        userService.createUser(CreateUserRequest(name = "bill", email = "email2")) flatMap { _ =>
          recoverToExceptionIf[StatusRuntimeException] {
            userService.listUsers(ListUsersRequest(pageSize = 1, pageToken = "qwerty"))
          } map { exception => exception.getStatus.getCode shouldBe Status.INVALID_ARGUMENT.getCode }
        }
      }
    }
  }

  def createUserAndAccount(name: String = "adam", email: String = "email", income: Int = 5000, expenses: Int = 0)
                          (implicit userService: UserServiceImpl, accountService: AccountServiceImpl): Future[CreateAccountResponse] = {
    userService.createUser(CreateUserRequest(name, email, income, expenses)) flatMap { user =>
      accountService.createAccount(CreateAccountRequest(user.id))
    }
  }

  def all(assertion: Assertion*): Assertion = assert(assertion.forall(_ == Succeeded))
}
