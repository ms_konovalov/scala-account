package ms.konovalov.zip

import java.util.UUID

import io.grpc.{Status, StatusRuntimeException}
import org.scalatest.{Assertion, AsyncFreeSpec, Matchers, Succeeded}

import scala.concurrent.ExecutionContext

class UserServiceTest extends AsyncFreeSpec with Matchers {

  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global

  "create user" - {
    "should create a user with id" in {
      val userDao = new UserDaoFake
      val userService: UserServiceImpl = new UserServiceImpl(userDao)
      userService.createUser(CreateUserRequest(name = "adam", email = "email")) map { result =>
        result.id.length should be > 0
      }
    }

    "should fail for user with existing email" in {
      val userDao = new UserDaoFake
      val userService: UserServiceImpl = new UserServiceImpl(userDao)
      recoverToExceptionIf[StatusRuntimeException] {
        userService.createUser(CreateUserRequest(name = "adam", email = "email")) flatMap { _ =>
          userService.createUser(CreateUserRequest(name = "bill", email = "email"))
        }
      } map { exception => exception.getStatus.getCode shouldBe Status.ALREADY_EXISTS.getCode }
    }
  }

  "get user" - {
    "should return created user with id" in {
      val userDao = new UserDaoFake
      val userService: UserServiceImpl = new UserServiceImpl(userDao)
      userService.createUser(CreateUserRequest(name = "adam", email = "email")) flatMap { created =>
        userService.getUser(GetUserRequest(created.id)) map { result =>
          all(
            result.id.length should be > 0,
            result.name shouldBe "adam"
          )
        }
      }
    }

    "should return NOT_FOUND for non-existent user" in {
      val userDao = new UserDaoFake
      val userService: UserServiceImpl = new UserServiceImpl(userDao)
      userService.createUser(CreateUserRequest(name = "adam", email = "email")) flatMap { _ =>
        userService.createUser(CreateUserRequest(name = "bill", email = "email2")) flatMap { _ =>
          recoverToExceptionIf[StatusRuntimeException] {
            userService.getUser(GetUserRequest(UUID.randomUUID().toString))
          } map { exception => exception.getStatus.getCode shouldBe Status.NOT_FOUND.getCode }
        }
      }
    }
  }

  "list users" - {
    "should be empty for empty data" in {
      val userDao = new UserDaoFake
      val userService: UserServiceImpl = new UserServiceImpl(userDao)
      userService.listUsers(ListUsersRequest(pageSize = 5)) map { result =>
        all(
          result.users should be(empty),
          result.nextPageToken shouldBe ""
        )
      }
    }

    "should return all data if the page size is bigger" in {
      val userDao = new UserDaoFake
      val userService: UserServiceImpl = new UserServiceImpl(userDao)
      userService.createUser(CreateUserRequest(name = "adam", email = "email")) flatMap { _ =>
        userService.createUser(CreateUserRequest(name = "bill", email = "email2")) flatMap { _ =>
          userService.listUsers(ListUsersRequest(pageSize = 5)) map { result =>
            all(
              result.users should have size 2,
              result.nextPageToken shouldBe ""
            )
          }
        }
      }
    }

    "should return data for first page" in {
      val userDao = new UserDaoFake
      val userService: UserServiceImpl = new UserServiceImpl(userDao)
      userService.createUser(CreateUserRequest(name = "adam", email = "email")) flatMap { _ =>
        userService.createUser(CreateUserRequest(name = "bill", email = "email2")) flatMap { _ =>
          userService.createUser(CreateUserRequest(name = "cam", email = "email3")) flatMap { _ =>
            userService.createUser(CreateUserRequest(name = "dan", email = "email4")) flatMap { _ =>
              userService.listUsers(ListUsersRequest(pageSize = 2)) map { result =>
                result.users should have size 2
              }
            }
          }
        }
      }
    }

    "should return data for next page" in {
      val userDao = new UserDaoFake
      val userService: UserServiceImpl = new UserServiceImpl(userDao)
      userService.createUser(CreateUserRequest(name = "adam", email = "email")) flatMap { _ =>
        userService.createUser(CreateUserRequest(name = "bill", email = "email2")) flatMap { _ =>
          userService.createUser(CreateUserRequest(name = "cam", email = "email3")) flatMap { _ =>
            userService.createUser(CreateUserRequest(name = "dan", email = "email4")) flatMap { _ =>
              userService.createUser(CreateUserRequest(name = "ellie", email = "email5")) flatMap { _ =>
                userService.listUsers(ListUsersRequest(pageSize = 2)) flatMap { page1 =>
                  userService.listUsers(ListUsersRequest(pageSize = 2, pageToken = page1.nextPageToken)) flatMap { page2 =>
                    userService.listUsers(ListUsersRequest(pageSize = 2, pageToken = page2.nextPageToken)) map { page3 =>
                      all(
                        page1.users should have size 2,
                        page2.users should have size 2,
                        page3.users should have size 1,
                        page2.users should not contain page1.users(0),
                        page2.users should not contain page1.users(1),
                        page2.users should not contain page3.users(0),
                        page1.users should not contain page3.users(0),
                      )
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    "should fail on incorrect page token" in {
      val userDao = new UserDaoFake
      val userService: UserServiceImpl = new UserServiceImpl(userDao)
      userService.createUser(CreateUserRequest(name = "adam", email = "email")) flatMap { _ =>
        userService.createUser(CreateUserRequest(name = "bill", email = "email2")) flatMap { _ =>
          recoverToExceptionIf[StatusRuntimeException] {
            userService.listUsers(ListUsersRequest(pageSize = 1, pageToken = "qwerty"))
          } map { exception => exception.getStatus.getCode shouldBe Status.INVALID_ARGUMENT.getCode }
        }
      }
    }
  }

  def all(assertion: Assertion*): Assertion = assert(assertion.forall(_ == Succeeded))
}
