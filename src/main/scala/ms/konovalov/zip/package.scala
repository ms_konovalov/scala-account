package ms.konovalov

import java.util.Base64

package object zip {

  /**
   * Decode base64 token
   * @param token encoded token
   * @return decoded token
   */
  def parseToken(token: String): String =
    Seq(token).filterNot(_.isEmpty).map(Base64.getDecoder.decode(token).map(_.toChar).mkString).headOption.getOrElse("")

  /**
   * Encode token as Base64
   * @param value decoded token as [[String]]
   * @return token encoded
   */
  def encodeToken(value: String): String = Base64.getEncoder.encode(value.getBytes).map(_.toChar).mkString
}
