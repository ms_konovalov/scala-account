package ms.konovalov.zip

import java.util.UUID

import io.grpc.Status
import reactivemongo.api.bson.collection.BSONCollection
import reactivemongo.api.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter, BSONObjectID, Macros}
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.{Cursor, MongoConnection, MongoDriver}

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/**
 * Interface for persistence operations with [[User]] data
 */
private[zip] trait UserDao {
  def verifyEmail(email: String): Future[Unit]

  def listUsers(perPage: Int, token: String): Future[(Seq[User], String)]

  def getUser(id: String): Future[User]

  def createUser(name: String, email: String, monthlyIncome: Int, monthlyExpenses: Int): Future[User]
}

/**
 * In-memory implementation of [[UserDao]]
 * Uses separate HashSet for checking for email uniqueness
 * !!! Is not thread-safe
 */
private[zip] class UserDaoFake extends UserDao {

  private val map: mutable.Map[String, User] = mutable.SortedMap()
  private val emails: mutable.Set[String] = mutable.Set()

  def verifyEmail(email: String): Future[Unit] = {
    if (emails.contains(email)) {
      return Future.failed(Status.ALREADY_EXISTS.augmentDescription(
        s"The user with email $email already exists").asRuntimeException())
    }
    Future.unit
  }

  def listUsers(perPage: Int, token: String): Future[(Seq[User], String)] = {
    (if (token.isEmpty) Some(0) else token.toIntOption) map { processedToken =>
      val users = map.values.slice(processedToken, processedToken + perPage).toSeq
      Future.successful(users, if (users.size < perPage) "" else s"${processedToken + perPage}")
    } getOrElse {
      Future.failed(Status.INVALID_ARGUMENT.augmentDescription(
        s"Next page token is invalid").asRuntimeException())
    }
  }

  def getUser(id: String): Future[User] =
    map.get(id) map {
      Future.successful
    } getOrElse {
      Future.failed(Status.NOT_FOUND.augmentDescription(
        s"The user with id $id is not found").asRuntimeException())
    }

  def createUser(name: String, email: String, monthlyIncome: Int, monthlyExpenses: Int): Future[User] = {
    val user = User(UUID.randomUUID().toString, name, email, monthlyIncome, monthlyExpenses)
    map.put(user.id, user)
    emails.add(user.email)
    Future.successful(user)
  }
}

/**
 * Interface for persistence operations with [[Account]] data
 */
private[zip] trait AccountDao {
  def checkDoesntExist(userId: String): Future[Unit]

  def createAccount(user: User): Future[Account]

  def listAccounts(perPage: Int, token: String): Future[(Seq[Account], String)]
}

/**
 * In-memory implementation for [[AccountDao]]
 */
private[zip] class AccountDaoFake extends AccountDao {

  private val map: mutable.HashMap[String, Account] = mutable.HashMap()

  def checkDoesntExist(userId: String): Future[Unit] = {
    map.get(userId) map { _ =>
      Future.failed(Status.ALREADY_EXISTS.augmentDescription(
        s"The account for user with userId $userId already exists").asRuntimeException())
    } getOrElse {
      Future.unit
    }
  }

  def createAccount(user: User): Future[Account] = {
    val account = Account(user.id, user)
    map.put(user.id, account)
    Future.successful(account)
  }

  def listAccounts(perPage: Int, token: String): Future[(Seq[Account], String)] = {
    (if (token.isEmpty) Some(0) else token.toIntOption) map { processedToken =>
      val accounts = map.values.slice(processedToken, processedToken + perPage).toSeq
      Future.successful(accounts, if (accounts.size < perPage) "" else s"${processedToken + perPage}")
    } getOrElse {
      Future.failed(Status.INVALID_ARGUMENT.augmentDescription(
        s"Next page token is invalid").asRuntimeException())
    }
  }
}

/**
 * Dao implementation for MongoDB based on ReactiveMongo
 * @param mongoUri connection string
 * @param databaseName database name, is used to provide separate dbs for different integration tests
 * @param executionContext context for [[Future]]s
 */
private[zip] class DaoMongo(mongoUri: String, databaseName: String = "account")
                           (implicit val executionContext: ExecutionContext) extends UserDao with AccountDao {
  private val driver = MongoDriver()
  private val connection: Try[MongoConnection] = MongoConnection.parseURI(mongoUri).flatMap(uri => driver.connection(uri, strictUri = false))
  private val database = Future.fromTry(connection).flatMap(_.database(databaseName))

  def userCollection: Future[BSONCollection] = database.map(_.collection("user"))

  implicit def userWriter: BSONDocumentWriter[User] = Macros.writer[User]

  implicit object UserWriter extends BSONDocumentWriter[User] {
    override def writeTry(user: User): Try[BSONDocument] = {
      BSONObjectID.parse(user.id) map { id =>
        BSONDocument(
          "_id" -> id,
          "name" -> user.name,
          "email" -> user.email,
          "monthlyIncome" -> user.monthlyIncome,
          "monthlyExpenses" -> user.monthlyExpenses)
      }
    }
  }

  implicit object UserReader extends BSONDocumentReader[User] {
    def readDocument(doc: BSONDocument): Try[User] = {
      doc.getAsTry[BSONObjectID]("_id") flatMap { id =>
        doc.getAsTry[String]("name") flatMap { name =>
          doc.getAsTry[String]("email") flatMap { email =>
            doc.getAsTry[Int]("monthlyIncome") flatMap { income =>
              doc.getAsTry[Int]("monthlyExpenses") flatMap { expenses =>
                Success(User(id.stringify, name, email, income, expenses))
              }
            }
          }
        }
      }
    }
  }

  implicit object AccountReader extends BSONDocumentReader[Account] {
    def readDocument(doc: BSONDocument): Try[Account] = {
      doc.getAsTry[BSONObjectID]("_id") flatMap { id =>
        doc.getAsTry[String]("name") flatMap { name =>
          doc.getAsTry[String]("email") flatMap { email =>
            doc.getAsTry[Int]("monthlyIncome") flatMap { income =>
              doc.getAsTry[Int]("monthlyExpenses") flatMap { expenses =>
                Success(Account(id.stringify, User(id.stringify, name, email, income, expenses)))
              }
            }
          }
        }
      }
    }
  }

  def createUser(name: String, email: String, monthlyIncome: Int, monthlyExpenses: Int): Future[User] = {
    val id = BSONObjectID.generate
    val user = User(id.stringify, name, email, monthlyIncome, monthlyExpenses)
    userCollection.flatMap(_.insert.one(user)) flatMap { result: WriteResult =>
      if (result.ok)
        Future.successful(User(id.stringify, name, email, monthlyIncome, monthlyExpenses))
      else
        Future.failed(Status.INTERNAL.augmentDescription("Error during insertion into DB").asRuntimeException())
    }
  }

  def getUser(id: String): Future[User] = {
    BSONObjectID.parse(id) map { objectId =>
      val query = BSONDocument("_id" -> BSONDocument("$eq" -> objectId))
      userCollection.flatMap(_.find(query, None).one[User]) flatMap {
        _.map {
          Future.successful
        } getOrElse {
          Future.failed(Status.NOT_FOUND.augmentDescription(s"The user with id $id is not found").asRuntimeException())
        }
      }
    }
  } match {
    case Success(x) => x
    case Failure(_) =>
      Future.failed(Status.INVALID_ARGUMENT.augmentDescription(s"Provided user id $id is invalid").asRuntimeException())
  }

  def verifyEmail(email: String): Future[Unit] = {
    val query = BSONDocument("email" -> BSONDocument("$eq" -> email))
    userCollection.flatMap(_.find(query, None).one[User]) flatMap {
      _.map { _ =>
        Future.failed(Status.ALREADY_EXISTS.augmentDescription(
          s"The user with email $email already exists").asRuntimeException())
      } getOrElse {
        Future.unit
      }
    }
  }

  def listUsers(perPage: Int, token: String): Future[(Seq[User], String)] = {
    createListQuery(token) map { query =>
      userCollection.flatMap(_.find(query, None).batchSize(perPage).cursor[User]().collect[Seq](perPage, Cursor.FailOnError[Seq[User]]())) map {
        res => (res, if (res.size < perPage) "" else res.last.id)
      }
    } match {
      case Success(future) => future
      case Failure(_: IllegalArgumentException) => Future.failed(Status.INVALID_ARGUMENT.augmentDescription(
        s"Next page token is invalid").asRuntimeException())
      case Failure(_) => Future.failed(Status.INTERNAL.augmentDescription(
        s"Something went wrong").asRuntimeException())
    }
  }

  def checkDoesntExist(userId: String): Future[Unit] = {
    Future.unit
  }

  def createAccount(user: User): Future[Account] = {
    (BSONObjectID.parse(user.id) match {
      case Success(userId) => Future.successful(userId)
      case Failure(_: IllegalArgumentException) => Future.failed(Status.INVALID_ARGUMENT.augmentDescription(
        s"Provided user id is invalid").asRuntimeException())
      case Failure(_) => Future.failed(Status.INTERNAL.augmentDescription(
        s"Something went wrong").asRuntimeException())
    }) flatMap { userId =>
      val selector = BSONDocument("_id" -> BSONDocument("$eq" -> userId), "account" -> BSONDocument("$exists" -> false))
      val updater = BSONDocument("$set" -> BSONDocument("account" -> userId))
      userCollection.flatMap(_.findAndUpdate(selector, updater, fetchNewObject = true, upsert = false))
    } flatMap { result =>
      if (result.value.isDefined)
        Future.successful(Account(user.id, user))
      else
        Future.failed(Status.INTERNAL.augmentDescription("Error during insertion into DB").asRuntimeException())
    }
  }

  def listAccounts(perPage: Int, token: String): Future[(Seq[Account], String)] =
    createListQuery(token) map { query =>
      userCollection.flatMap(_.find(query, None).batchSize(perPage).cursor[Account]().collect[Seq](perPage, Cursor.FailOnError[Seq[Account]]())) map {
        res => (res, if (res.size < perPage) "" else res.last.userId)
      }
    } match {
      case Success(future) => future
      case Failure(_: IllegalArgumentException) => Future.failed(Status.INVALID_ARGUMENT.augmentDescription(
        s"Next page token is invalid").asRuntimeException())
      case Failure(_) => Future.failed(Status.INTERNAL.augmentDescription(
        s"Something went wrong").asRuntimeException())
    }

  private def createListQuery(token: String): Try[BSONDocument] =
    if (token.isEmpty) Success(BSONDocument()) else BSONObjectID.parse(token) map { from =>
      BSONDocument("_id" -> BSONDocument("$gt" -> from))
    }

  def stop(): Unit = driver.close()

}
