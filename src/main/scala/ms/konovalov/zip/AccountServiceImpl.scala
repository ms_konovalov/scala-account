package ms.konovalov.zip

import io.grpc.Status

import scala.concurrent.{ExecutionContext, Future}

/**
 * Internal representation of Accout entity, is used for persistence serialization
 * @param userId id of user
 * @param user user data as [[User]]
 */
private[zip] case class Account(userId: String, user: User)

/**
 * Implementation of the service methods related to [[Account]] entity
 * @param userDao dao for working with [[User]] data
 * @param accountDao dao for working with [[Account]] data
 * @param executionContext context for working with [[Future]]s
 */
private[zip] class AccountServiceImpl(userDao: UserDao, accountDao: AccountDao)
                                (implicit val executionContext: ExecutionContext) extends AccountServiceGrpc.AccountService {

  /**
   * Async create an account for provided user. Verifies that account doesn't exist for the user and
   * verifies that user has enough income (see [[verifyIncome()]] and then creates the account
   * @param request request object containing user id (see [[CreateAccountRequest.userId]])
   * @return async result of the creation as a [[Future.successful]] or [[Future.failed]] in case of errors
   */
  override def createAccount(request: CreateAccountRequest): Future[CreateAccountResponse] =
    userDao.getUser(request.userId) flatMap { user =>
      accountDao.checkDoesntExist(user.id) flatMap { _ =>
        verifyIncome(user.monthlyIncome, user.monthlyExpenses) flatMap { _ =>
          accountDao.createAccount(user) map { account =>
            CreateAccountResponse(account.userId)
          }
        }
      }
    }

  /**
   * Async verify if the monthly income is 1000 higher than expenses
   * @param income monthly income
   * @param expenses monthly expenses
   * @return async result of the creation as a [[Future.successful]] or [[Future.failed]] in case of errors
   */
  private def verifyIncome(income: Int, expenses: Int): Future[Unit] = {
    if (income - expenses < 1000) {
      return Future.failed(Status.FAILED_PRECONDITION.augmentDescription(
        "Monthly income for the user is less than 1000$").asRuntimeException())
    }
    Future.unit
  }

  /**
   * Async queries paginated list of accounts with [[ListAccountRequest.pageSize]] as a page size
   * and [[ListAccountRequest.pageToken]] as a next page token provided in the previous request or empty string
   * Sorting is not implemented
   * @param request request object for paginated query
   * @return async result of the creation as a [[Future.successful]] or [[Future.failed]] in case of errors
   */
  override def listAccount(request: ListAccountRequest): Future[ListAccountResponse] = {
    val parsedToken = parseToken(request.pageToken)
    accountDao.listAccounts(request.pageSize, parsedToken) map { case (accounts, token) =>
      ListAccountResponse(convertToResponse(accounts), encodeToken(token))
    }
  }

  private def convertToResponse(accounts: Seq[Account]): Seq[CreateAccountResponse] =
    accounts map { account =>
      CreateAccountResponse(account.userId,
        Some(GetUserResponse(account.user.id, account.user.name, account.user.email, account.user.monthlyIncome, account.user.monthlyExpenses))
      )
    }
}
