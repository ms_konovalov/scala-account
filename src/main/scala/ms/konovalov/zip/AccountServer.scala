package ms.konovalov.zip

import io.grpc.netty.NettyServerBuilder
import io.grpc.{Server, ServerBuilder}

import scala.concurrent.ExecutionContext

/**
 * gRPC server
 *
 * @param port             port to listen
 * @param mongoUri         connection string to mongoDB
 * @param inMemory         flag to use in-memory storage implementation instead of MongoDB
 * @param executionContext context for working with futures
 */
class AccountServer(port: Int, mongoUri: String, inMemory: Boolean = false)
                   (implicit executionContext: ExecutionContext) { self =>

  private var server: Option[Server] = None

  private val (userDao, accountDao) = initDao
  private val userService = new UserServiceImpl(userDao)
  private val accountService = new AccountServiceImpl(userDao, accountDao)

  private def start(): Unit = {
    server = Some(
      ServerBuilder.forPort(port).asInstanceOf[ServerBuilder[NettyServerBuilder]]
        .addService(AccountServiceGrpc.bindService(accountService, executionContext))
        .addService(UserServiceGrpc.bindService(userService, executionContext))
        .build.start
    )
    println("Server started, listening on " + port)
    sys.addShutdownHook {
      println("*** shutting down gRPC server since JVM is shutting down")
      self.stop()
      println("*** server shut down")
    }
  }

  private def initDao = {
    if (inMemory) {
      (new UserDaoFake, new AccountDaoFake)
    } else {
      val dao = new DaoMongo(mongoUri)
      (dao, dao)
    }
  }

  private def stop() = server.map(_.shutdown())

  private def blockUntilShutdown() = server.map(_.awaitTermination())

}

/**
 * Companion object for initialising service, entry point to the server-side execution
 * Takes values from ENV or fills with default values:
 *    ZIP_PORT - port to listen, default 9080
 *    MONGO_URL - connection string to MongoDB, default is for localhost and port 27017
 *    USE_IN_MEMORY_STORE - use in-memory store implementation instead of MongoDB
 */
object AccountServer extends App {
  val port: Int = sys.env.get("ZIP_PORT").flatMap(_.toIntOption).getOrElse(9080)
  val mongoUrl: String = sys.env.getOrElse("MONGO_URL", "mongodb://localhost:27017/account?authenticationMechanism=scram-sha1")
  val inMemory: Boolean = sys.env.get("USE_IN_MEMORY_STORE").flatMap(_.toBooleanOption).getOrElse(false)
  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global
  val server = new AccountServer(port, mongoUrl, inMemory)
  server.start()
  server.blockUntilShutdown()
}
