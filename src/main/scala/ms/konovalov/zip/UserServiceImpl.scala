package ms.konovalov.zip

import scala.concurrent.{ExecutionContext, Future}
/**
 * Internal representation of User entity, is used for persistence serialization
 * @param id identifier
 * @param name name
 * @param email email
 * @param monthlyIncome monthly income
 * @param monthlyExpenses monthly expenses
 */
private[zip] case class User(id: String, name: String, email: String, monthlyIncome: Int, monthlyExpenses: Int)

/**
 * Implementation of the service methods related to [[User]] entity
 * @param userDao dao for working with [[User]] data
 * @param executionContext context for working with [[Future]]s
 */
private[zip] class UserServiceImpl(userDao: UserDao)
                                  (implicit val executionContext: ExecutionContext) extends UserServiceGrpc.UserService {

  /**
   * Async create user info checking for email uniqueness
   * Important! email uniqueness check is not atomic with the user creation
   * @param request user info
   * @return async response as [[Future.successful]] or [[Future.failed]] in case of errors
   */
  override def createUser(request: CreateUserRequest): Future[CreateUserResponse] =
    userDao.verifyEmail(request.email) flatMap { _ =>
      userDao.createUser(request.name, request.email, request.monthlyIncome, request.monthlyExpenses) map { user =>
        CreateUserResponse(user.id, user.name, user.email, user.monthlyIncome, user.monthlyExpenses)
      }
    }

  /**
   * Async returns user info
   * @param request identifier of the user
   * @return async response as [[Future.successful]] or [[Future.failed]] in case of errors
   */
  override def getUser(request: GetUserRequest): Future[GetUserResponse] =
    userDao.getUser(request.id) map { item =>
      GetUserResponse(request.id, item.name, item.email, item.monthlyIncome, item.monthlyExpenses)
    }

  /**
   * Async queries paginated list of users with [[ListUsersRequest.pageSize]] as a page size
   * and [[ListUsersRequest.pageToken]] as a next page token provided in the previous request or empty string
   * Sorting is not implemented
   * @param request request object for paginated query
   * @return async result of the creation as a [[Future.successful]] or [[Future.failed]] in case of errors
   */
  override def listUsers(request: ListUsersRequest): Future[ListUsersResponse] = {
    val parsedToken = parseToken(request.pageToken)
    userDao.listUsers(request.pageSize, parsedToken).map { case (users, token) =>
      ListUsersResponse(convertToResponse(users), encodeToken(token))
    }
  }

  private def convertToResponse(users: Seq[User]): Seq[GetUserResponse] =
    users.map(user => GetUserResponse(user.id, user.name, user.email, user.monthlyIncome, user.monthlyExpenses))
}
