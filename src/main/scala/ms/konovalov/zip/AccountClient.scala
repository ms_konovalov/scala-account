package ms.konovalov.zip

import java.util.concurrent.TimeUnit

import io.grpc.netty.NettyChannelBuilder
import io.grpc.{ManagedChannel, ManagedChannelBuilder}
import ms.konovalov.zip.AccountServiceGrpc.AccountServiceStub
import ms.konovalov.zip.UserServiceGrpc.UserServiceStub

import scala.concurrent.Future

/**
 * Companion object for initializing client-to-service gRPC connection
 */
object AccountClient {

  /**
   * Construct client
   * @param host host address to connect
   * @param port port to connect
   * @return instance of gRPC client
   */
  def apply(host: String, port: Int): AccountClient = {
    val channel = ManagedChannelBuilder.forAddress(host, port).asInstanceOf[ManagedChannelBuilder[NettyChannelBuilder]]
      .usePlaintext().build
    val accountClient = AccountServiceGrpc.stub(channel)
    val userClient = UserServiceGrpc.stub(channel)
    new AccountClient(channel, accountClient, userClient)
  }
}

/**
 * gPRC client for interaction with the service
 * @param channel Netty channel for network interaction
 * @param accountClient gRPC client for [[AccountServiceGrpc]] methods
 * @param userClient gRPC client for [[UserServiceGrpc]] methods
 */
class AccountClient(private val channel: ManagedChannel,
                    private val accountClient: AccountServiceStub,
                    private val userClient: UserServiceStub) {

  def shutdown(): Unit = channel.shutdown.awaitTermination(5, TimeUnit.SECONDS)

  def createAccount(request: CreateAccountRequest): Future[CreateAccountResponse] = accountClient.createAccount(request)

  def listAccount(request: ListAccountRequest): Future[ListAccountResponse] = accountClient.listAccount(request)

  def createUser(request: CreateUserRequest): Future[CreateUserResponse] = userClient.createUser(request)

  def getUser(request: GetUserRequest): Future[GetUserResponse] = userClient.getUser(request)

  def listUsers(request: ListUsersRequest): Future[ListUsersResponse] = userClient.listUsers(request)
}
