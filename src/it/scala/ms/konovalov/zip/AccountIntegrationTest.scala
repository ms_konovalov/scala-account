package ms.konovalov.zip

import java.util.concurrent.TimeUnit

import io.grpc.{Status, StatusRuntimeException}
import org.scalatest._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

@WrapWith(classOf[ConfigMapWrapperSuite])
class AccountIntegrationTest(configMap: Map[String, Any]) extends AsyncFreeSpec with Matchers with BeforeAndAfterAll {

  implicit private val ec: ExecutionContext = ExecutionContext.Implicits.global

  private val serviceKey = "mongo:27017"

  private def mongoUrl: String = s"mongodb://${configMap.getOrElse(serviceKey, "127.0.0.1:27017")}/account-test?authenticationMechanism=scram-sha1"

  val dao = new DaoMongo(mongoUrl, "account-test")
  val userService = new UserServiceImpl(dao)
  val accountService = new AccountServiceImpl(dao, dao)

  override protected def beforeAll(): Unit = {
    val result = for {
      u1 <- userService.createUser(CreateUserRequest(name = "adam", email = "email1"))
      u2 <- userService.createUser(CreateUserRequest(name = "bill", email = "email2"))
      u3 <- userService.createUser(CreateUserRequest(name = "cam", email = "email3", monthlyIncome = 3000))
      a3 <- accountService.createAccount(CreateAccountRequest(u3.id))
      u4 <- userService.createUser(CreateUserRequest(name = "dan", email = "email4"))
      u5 <- userService.createUser(CreateUserRequest(name = "evan", email = "email5"))
    } yield (u1, u2, u3, u4, u5, a3)
    Await.result(result, Duration(5, TimeUnit.SECONDS))
  }

  "create user" - {
    "should fail for user with existing email" in {
      recoverToExceptionIf[StatusRuntimeException] {
        userService.createUser(CreateUserRequest(name = "adam", email = "email1"))
      } map { exception => exception.getStatus.getCode shouldBe Status.ALREADY_EXISTS.getCode }
    }
  }

  "get user" - {
    "should return NOT_FOUND for non-existent user" in {
      recoverToExceptionIf[StatusRuntimeException] {
        userService.getUser(GetUserRequest("5de08aa57d88a84a03139db2"))
      } map { exception => exception.getStatus.getCode shouldBe Status.NOT_FOUND.getCode }
    }

    "should fail for invalid user id" in {
      recoverToExceptionIf[StatusRuntimeException] {
        userService.getUser(GetUserRequest("blablabla"))
      } map { exception => exception.getStatus.getCode shouldBe Status.INVALID_ARGUMENT.getCode }
    }

    "should return user" in {
      userService.listUsers(ListUsersRequest(pageSize = 1)) flatMap { users =>
        userService.getUser(GetUserRequest(users.users.head.id)) map { user =>
          assert(user.id == users.users.head.id)
        }
      }
    }
  }

  "list users" - {
    "should return data for first page" in {
      userService.listUsers(ListUsersRequest(pageSize = 2)) map { result =>
        result.users should have size 2
      }
    }

    "should return data for next page" in {
      userService.listUsers(ListUsersRequest(pageSize = 2)) flatMap { page1 =>
        userService.listUsers(ListUsersRequest(pageSize = 2, pageToken = page1.nextPageToken)) flatMap { page2 =>
          userService.listUsers(ListUsersRequest(pageSize = 2, pageToken = page2.nextPageToken)) map { page3 =>
            all(
              page1.users should have size 2,
              page2.users should have size 2,
              page3.users should have size 1,
              page2.users should not contain page1.users(0),
              page2.users should not contain page1.users(1),
              page2.users should not contain page3.users(0),
              page1.users should not contain page3.users(0),
            )
          }
        }
      }
    }
  }

  "create account" - {
    "should fail to create account for the non-existent user" in {
      recoverToExceptionIf[StatusRuntimeException] {
        accountService.createAccount(CreateAccountRequest("5de08aa57d88a84a03139db2"))
      } map { exception => exception.getStatus.getCode shouldBe Status.NOT_FOUND.getCode }
    }

    "should fail for invalid user id" in {
      recoverToExceptionIf[StatusRuntimeException] {
        accountService.createAccount(CreateAccountRequest("blablabla"))
      } map { exception => exception.getStatus.getCode shouldBe Status.INVALID_ARGUMENT.getCode }
    }

    "should fail for user with not enough income" in {
      recoverToExceptionIf[StatusRuntimeException] {
        userService.listUsers(ListUsersRequest(pageSize = 1)) flatMap { user =>
          accountService.createAccount(CreateAccountRequest(user.users.head.id)) map { result =>
            result.id should not be empty
          }
        }
      } map { exception => exception.getStatus.getCode shouldBe Status.FAILED_PRECONDITION.getCode }
    }

    "should create account for user with not enough income" in {
      userService.createUser(CreateUserRequest("qwentin", "email100", 3000, 100)) flatMap { user =>
        accountService.createAccount(CreateAccountRequest(user.id)) map { result =>
          result.id should not be empty
        }
      }
    }
  }

  "list account" - {
    "should return list of accounts" in {
      accountService.listAccount(ListAccountRequest(pageSize = 1)) map { accounts =>
        accounts.accounts should have size 1
      }
    }
  }

  override protected def afterAll(): Unit = dao.stop()

  def all(assertion: Assertion*): Assertion = assert(assertion.forall(_ == Succeeded))
}
