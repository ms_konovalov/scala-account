import java.io.File

resolvers += "Artima Maven Repository" at "https://repo.artima.com/releases"

name := "zip"

version := "0.1"

scalaVersion := "2.13.1"

PB.targets in Compile := Seq(
  scalapb.gen(javaConversions = false, grpc = true, flatPackage = true) -> (sourceManaged in Compile).value
)

libraryDependencies ++= Seq(
  "io.grpc" % "grpc-netty" % scalapb.compiler.Version.grpcJavaVersion,
  "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapb.compiler.Version.scalapbVersion,
  "org.reactivemongo" %% "reactivemongo" % "0.19.1",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "org.apache.logging.log4j" % "log4j-core" % "2.12.1",
  "org.scalatest" %% "scalatest" % "3.0.8" % "it,test",
  "org.slf4j" % "slf4j-simple" % "1.7.29" % "it,test"
)

val mainClassStr = "ms.konovalov.zip.AccountServer"
mainClass in(Compile, run) := Some(mainClassStr)

Defaults.itSettings

//parallelExecution in Test := false

lazy val `zip` = (project in file(".")).configs(IntegrationTest)

enablePlugins(JavaAppPackaging, DockerPlugin, DockerComposePlugin, AshScriptPlugin)

dockerBaseImage := "openjdk:jre-alpine"
dockerExposedPorts ++= Seq(9080)

dockerImageCreationTask := (publishLocal in Docker).value

//To use 'dockerComposeTest' to run tests in the 'IntegrationTest' scope instead of the default 'Test' scope:
// 1) Package the tests that exist in the IntegrationTest scope
testCasesPackageTask := (sbt.Keys.packageBin in IntegrationTest).value
// 2) Specify the path to the IntegrationTest jar produced in Step 1
testCasesJar := artifactPath.in(IntegrationTest, packageBin).value.getAbsolutePath
// 3) Include any IntegrationTest scoped resources on the classpath if they are used in the tests
testDependenciesClasspath := {
  val fullClasspathCompile = (fullClasspath in Compile).value
  val classpathTestManaged = (managedClasspath in IntegrationTest).value
  val classpathTestUnmanaged = (unmanagedClasspath in IntegrationTest).value
  val testResources = (resources in IntegrationTest).value
  (fullClasspathCompile.files ++ classpathTestManaged.files ++ classpathTestUnmanaged.files ++ testResources).map(_.getAbsoluteFile).mkString(File.pathSeparator)
}
composeContainerPauseBeforeTestSeconds := 10
