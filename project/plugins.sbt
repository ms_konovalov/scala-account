resolvers += "Artima Maven Repository" at "https://repo.artima.com/releases"

addSbtPlugin("com.thesamet" % "sbt-protoc" % "0.99.27")
addSbtPlugin("com.artima.supersafe" % "sbtplugin" % "1.1.8")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.5.1")
addSbtPlugin("com.tapad" % "sbt-docker-compose" % "1.0.35")

libraryDependencies += "com.thesamet.scalapb" %% "compilerplugin" % "0.9.4"
